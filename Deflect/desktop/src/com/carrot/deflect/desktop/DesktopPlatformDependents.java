package com.carrot.deflect.desktop;

import com.carrot.deflect.service.PlatformDependents;
import com.carrot.deflect.service.Logger;

/**
 * Created by aharm on 9/22/2017.
 */

public class DesktopPlatformDependents extends PlatformDependents{

    public DesktopPlatformDependents()
    {
        logger = new Logger();
        platform = "desktop";
        debug = false;
    }
}
