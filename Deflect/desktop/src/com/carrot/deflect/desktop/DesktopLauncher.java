package com.carrot.deflect.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.carrot.deflect.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		
		config.width = 1280;
		config.height = 720;
		config.title = Game.TITLE;
		config.samples = 4;
		
		new LwjglApplication(new Game(new DesktopPlatformDependents()), config);
	}
}
