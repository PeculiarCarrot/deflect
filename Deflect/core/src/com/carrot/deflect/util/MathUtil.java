package com.carrot.deflect.util;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by aharm on 9/24/2017.
 */

public class MathUtil {

	public static boolean isDegreeAngleBetween(float targetAngle, float a, float b)
	{
		targetAngle = (360 + (targetAngle % 360)) % 360;
		a = (3600000 + a) % 360;
		b = (3600000 + b) % 360;

		if (a < b)
			return a <= targetAngle && targetAngle <= b;
		return a <= targetAngle || targetAngle <= b;
	}

	public static float magnitude(Vector2 v)
	{
		return (float) Math.sqrt(v.x * v.x + v.y * v.y);
	}

	public static float distance(Vector2 p1, Vector2 p2)
	{
		return MathUtil.distance(p1.x, p1.y, p2.x, p2.y);
	}

	public static float distance(float x1, float y1, float x2, float y2)
	{
		return (float) Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}
}
