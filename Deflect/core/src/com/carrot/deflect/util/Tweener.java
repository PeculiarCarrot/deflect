package com.carrot.deflect.util;

import com.carrot.deflect.entity.Entity;
import com.carrot.deflect.entity.EntityAccessor;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

/**
 * Created by aharm on 9/29/2017.
 */

public class Tweener {

	private static TweenManager tweenManager;

	static {
		Tween.registerAccessor(Entity.class, new
				EntityAccessor());
		tweenManager = new TweenManager();
	}

	public static TweenManager getManager()
	{
		return tweenManager;
	}
}
