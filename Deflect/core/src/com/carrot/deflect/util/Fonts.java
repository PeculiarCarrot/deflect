package com.carrot.deflect.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

/**
 * Created by aharm on 9/28/2017.
 */

public class Fonts {

	public static BitmapFont score;

	static {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("trench.ttf"));
		FreeTypeFontParameter params = new FreeTypeFontParameter();

		params.size = 50;
		params.flip = true;
		score = generator.generateFont(params);

		generator.dispose();
	}
}
