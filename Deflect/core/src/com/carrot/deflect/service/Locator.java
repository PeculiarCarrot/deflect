package com.carrot.deflect.service;

import java.util.Random;

/**
 * Locates various utilities for use across the engine.
 * @author Austin Harman
 */
public class Locator {
	
	private static Animator animator;
	private static Logger logger;
	private static GameStateManager stateManager;
	private static Audio audio;
	private static Random rng;
	
	public static void init(PlatformDependents dependents)
	{
		animator = new Animator().init();
		logger = dependents.getLogger().init();
		stateManager = new GameStateManager().init();
		audio = new Audio().init();
		rng = new Random();
	}
	
	public static GameStateManager getStateManager()
	{
		return stateManager;
	}
	
	public static Logger getLogger()
	{
		return logger;
	}

	public static Animator getAnimator()
	{
		return animator;
	}

	public static Audio getAudio()
	{
		return audio;
	}

	public static Random getRNG()
	{
		return rng;
	}
	
	public static void dispose()
	{
		animator.dispose();
		logger.dispose();
		audio.dispose();
	}
	
}
