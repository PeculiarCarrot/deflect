package com.carrot.deflect.service;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;

/**
 * Created by aharm on 10/1/2017.
 */

public class Audio {

	private HashMap<String, Sound> sounds = new HashMap<String, Sound>();

	public Audio init()
	{
		sounds.put("ballHit", load("ballHit.wav"));
		sounds.put("ballHit2", load("ballHit2.wav"));
		sounds.put("ballHit3", load("ballHit3.wav"));
		sounds.put("shieldHit", load("shieldHit.wav"));
		sounds.put("die", load("die.wav"));
		sounds.put("die2", load("die2.wav"));
		sounds.put("die3", load("die3.wav"));
		return this;
	}


	public void play(String... names)
	{
		String name = names[names.length == 1 ? 0 : Locator.getRNG().nextInt(names.length)];
		if(sounds.keySet().contains(name))
		{
			sounds.get(name).play(.25f);
		}
		else
		{
			Locator.getLogger().error("No sound found with name "+name);
		}
	}

	public void loop(String name)
	{
		if(sounds.keySet().contains(name))
		{
			sounds.get(name).loop(.5f);
		}
		else
		{
			Locator.getLogger().error("No sound found with name "+name);
		}
	}

	public void stop(String name)
	{
		if(sounds.keySet().contains(name))
		{
			sounds.get(name).stop();
		}
		else
		{
			Locator.getLogger().error("No sound found with name "+name);
		}
	}

	private Sound load(String name)
	{
		return Gdx.audio.newSound(Gdx.files.internal("sounds/"+name));
	}

	public void dispose()
	{
		for(String s: sounds.keySet())
			sounds.get(s).dispose();
	}
}
