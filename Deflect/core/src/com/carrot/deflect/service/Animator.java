package com.carrot.deflect.service;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
/**
 * Used to create and find animations
 * @author Austin Harman
 *
 */
public class Animator {
	
	private HashMap<String,Animation<TextureRegion>> animations = new HashMap<String, Animation<TextureRegion>>();
	
	public Animation<TextureRegion> getAnimation(String name)
	{
		if(animations.containsKey(name))
			return cloneAnimation(animations.get(name));
		
		System.out.println("No animation with the name " + name);
		return null;
	}
	
	/**
	 * @param name The name used to find the animation
	 * @param texture The spritesheet to pull the images from
	 * @param tileWidth The width of the frames
	 * @param tileHeight The height of the frames
	 * @param frameDuration How long each frame will last (in seconds)
	 */
	public void addAnimation(String name, Texture texture, int tileWidth, int tileHeight, float frameDuration)
	{
		animations.put(name, newAnimation(getSheetFrames(texture, tileWidth, tileHeight), frameDuration));
	}
	
	public Animation<TextureRegion> cloneAnimation(Animation<TextureRegion> animation)
	{
		return new Animation<TextureRegion>(animation.getFrameDuration(), animation.getKeyFrames());
	}
	
	public void dispose()
	{
		animations.clear();
	}
	
	public Animator init()
	{
		addAnimation("Sam_Idle", new Texture(Gdx.files.internal("textures/sam_idle.png")), 12, 12, 1);
		addAnimation("Sam_Run", new Texture(Gdx.files.internal("textures/sam_run.png")), 12, 12, .25f);
		
		return this;
	}
	
	private TextureRegion[] getSheetFrames(Texture texture, int tileWidth, int tileHeight)
	{
		TextureRegion[][] tmp = TextureRegion.split(texture, 
				tileWidth,
				tileHeight);

		// Puts the texture regions from a 2D array into a 1D array
		TextureRegion[] frames = new TextureRegion[(texture.getWidth() / tileWidth) * (texture.getHeight() / tileHeight)];
		int index = 0;
		for (int i = 0; i < (texture.getHeight() / tileHeight); i++)
		{
			for (int j = 0; j < (texture.getWidth() / tileWidth); j++)
			{
				frames[index++] = tmp[i][j];
			}
		}
		return frames;
	}
	
	private Animation<TextureRegion> newAnimation(TextureRegion[] frames, float frameDuration)
	{
		return new Animation<TextureRegion> (frameDuration, frames);
	}

}
