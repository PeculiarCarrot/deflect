package com.carrot.deflect.service;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.carrot.deflect.state.State;
import com.carrot.deflect.util.render.ShapeRenderer;

public class GameStateManager {
	
	private State currentState;
	private boolean justSwitchedState = false;
	
	public GameStateManager init()
	{
		return this;
	}
	
	public void set(State state)
	{
		if(currentState != null)
		{
			currentState.onExitState();
			currentState.dispose();
		}
		
		currentState = state;
		currentState.onEnterState();
		justSwitchedState = true;
	}
	
	public void update(float dt)
	{
		justSwitchedState = false;
		if(!justSwitchedState)
		{
			currentState.handleInput();
			currentState.update(dt);
		}
	}
	
	public void render(SpriteBatch sb, ShapeRenderer sr)
	{
		if(!justSwitchedState)
			currentState.render(sb, sr);
	}
	
	public void dispose()
	{
		if(currentState != null)
			currentState.dispose();
	}

}
