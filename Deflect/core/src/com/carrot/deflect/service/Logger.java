package com.carrot.deflect.service;

import java.util.Calendar;

/**
 * Logs info, warnings, and errors.
 * @author Austin Harman
 *
 */
public class Logger {
	
	public Logger init()
	{
		return this;
	}
	
	public void log(String log)
	{
		System.out.println(log+"    INFO["+Calendar.getInstance().getTime().toString()+"]");
	}
	
	public void error(String error)
	{
		System.out.print("ERROR["+Calendar.getInstance().getTime().toString()+"]: ");
		System.out.println(error);
		throwError();
	}
	
	public void warn(String warn)
	{
		System.out.print("WARN["+Calendar.getInstance().getTime().toString()+"]: ");
		System.out.println(warn);
	}
	
	public void throwError()
	{
		new Exception().printStackTrace();
	}
	
	public void dispose()
	{
		
	}

}
