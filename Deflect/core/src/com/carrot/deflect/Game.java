package com.carrot.deflect;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.carrot.deflect.service.Locator;
import com.carrot.deflect.service.PlatformDependents;
import com.carrot.deflect.state.MenuState;
import com.carrot.deflect.util.render.ShapeRenderer;

public class Game extends ApplicationAdapter {
	
	public static int WIDTH;
	public static int HEIGHT;
	public static final String TITLE = "Deflect";
	public static PlatformDependents platformDependents;
    public static boolean debug;
	
	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;
	private static Preferences prefs;
	private static int highScore;

	public Game(PlatformDependents dependents)
	{
		platformDependents = dependents;
        debug = platformDependents.debug;
	}

	public static void updateScore(int score)
	{
		highScore = prefs.getInteger("score", 0);
		if(score > highScore)
		{
			highScore = score;
			prefs.putInteger("score", score);
			prefs.flush();
		}
	}

	public static int getHighScore()
	{
		return highScore;
	}
	
	public void setDimensions(int width, int height)
	{
		Game.WIDTH = width;
		Game.HEIGHT = height;
	}
	
	@Override
	public void create () {

		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		Locator.init(platformDependents);
		Gdx.gl.glClearColor(0, 0, 0, 1);
        if(platformDependents.platform.equals("desktop"))
            setDimensions(1280, 720);
        else
        {
            setDimensions(Gdx.app.getGraphics().getWidth(),Gdx.app.getGraphics().getHeight());
            Locator.getLogger().log("Dimensions: " + Gdx.graphics.getWidth() + ", " + Gdx.graphics.getHeight());
            Locator.getLogger().log("Dimensions: " + Gdx.app.getGraphics().getWidth() + ", " + Gdx.app.getGraphics().getHeight());
        }
		Locator.getStateManager().set(new MenuState());
		prefs = Gdx.app.getPreferences("My Preferences");
		updateScore(0);
	}

	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Locator.getStateManager().update(Gdx.graphics.getDeltaTime());

        if(Gdx.input.isKeyJustPressed(Input.Keys.GRAVE))
            debug = !debug;

		Locator.getStateManager().render(batch, shapeRenderer);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		shapeRenderer.dispose();
		Locator.dispose();
	}
}
