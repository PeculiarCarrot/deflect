package com.carrot.deflect.entity;

import aurelienribon.tweenengine.TweenAccessor;

/**
 * This is used to tween the position, brightness, and scale of the entity.
 * Created by aharm on 9/29/2017.
 */

public class EntityAccessor implements TweenAccessor<Entity> {

	public static int POSITION = 0;
	public static int SCALE = 1;
	public static int BRIGHTNESS = 2;

	@Override
	public int getValues(Entity target, int tweenType, float[] returnValues) {
		if(tweenType == POSITION)
		{
			returnValues[0] = target.getPosition().x;
			returnValues[1] = target.getPosition().y;
			return 2;
		}
		else if(tweenType == SCALE)
		{
			returnValues[0] = target.getScale().x;
			returnValues[1] = target.getScale().y;
			return 2;
		}
		else if(tweenType == BRIGHTNESS)
		{
			returnValues[0] = target.getBrightness();
			return 1;
		}
		return 0;
	}

	@Override
	public void setValues(Entity target, int tweenType, float[] newValues) {
		if(tweenType == POSITION)
			target.getPosition().set(newValues[0], newValues[1]);
		else if(tweenType == SCALE)
			target.getScale().set(newValues[0], newValues[1]);
		else if(tweenType == BRIGHTNESS)
			target.setBrightness(newValues[0]);
	}
}
