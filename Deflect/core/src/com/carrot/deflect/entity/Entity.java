package com.carrot.deflect.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.carrot.deflect.util.render.ShapeRenderer;

public class Entity {
	
	private Vector2 position;
	private Vector2 velocity;
	private Vector2 scale;
	private float brightness;
	
	public Entity(int x, int y)
	{
		position = new Vector2(x, y);
		velocity = new Vector2();
		scale = new Vector2(1, 1);
	}
	
	public void init()
	{
		
	}

	public float getBrightness()
	{
		return brightness;
	}

	public void setBrightness(float brightness)
	{
		this.brightness = brightness;
	}

    public Vector2 getPosition() {
        return position;
    }

	public Vector2 getVelocity() {
		return velocity;
	}

	public Vector2 getScale() {
		return scale;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}
	
	public void update(float dt)
	{
		position.add(velocity.x * dt, velocity.y * dt);
	}
	
	public void render(SpriteBatch sb, ShapeRenderer sr)
	{
		
	}
	
	/**
	 * When the entity is destroyed, before being disposed
	 */
	public void destroy()
	{
		
	}
	
	/**
	 * Cleans up resources, namely textures
	 */
	public void dispose()
	{
		
	}

}
