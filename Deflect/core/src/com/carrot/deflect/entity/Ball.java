package com.carrot.deflect.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.carrot.deflect.Game;
import com.carrot.deflect.service.Locator;
import com.carrot.deflect.state.PlayState;
import com.carrot.deflect.util.MathUtil;
import com.carrot.deflect.util.Tweener;
import com.carrot.deflect.util.render.ShapeRenderer;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;

public class Ball extends Entity {

    private int radius = 9;
    private PlayState playState;

    public Ball(int x, int y, PlayState playState) {
        super(x, y);
        getVelocity().set((float) Math.random() * 500f, (float) Math.random() * 500f);
        this.playState = playState;
    }

    public void update(float dt) {
        if ((getPosition().x + radius >= Game.WIDTH && getVelocity().x > 0) || (getPosition().x - radius <= 0 && getVelocity().x < 0))
        {
			getVelocity().x *= -1;
			Locator.getAudio().play("ballHit","ballHit2","ballHit3");
		}
        if ((getPosition().y + radius >= Game.HEIGHT && getVelocity().y > 0) || (getPosition().y - radius <= 0 && getVelocity().y < 0))
        {
			getVelocity().y *= -1;
			Locator.getAudio().play("ballHit","ballHit2","ballHit3");
		}

        getPosition().add(getVelocity().x * dt * playState.getDifficulty(), getVelocity().y * dt * playState.getDifficulty());

		testBallCollision();
        testPlayerCollision();
    }

    public int getRadius()
	{
		return radius;
	}

    private void testPlayerCollision()
    {
		Player p = playState.getPlayer();

		if(p.shieldHits(this))
		{
			float velMagnitude = MathUtil.magnitude(getVelocity());
			double newAngle = Math.atan2(getPosition().y - p.getPosition().y, getPosition().x - p.getPosition().x);
			getVelocity().set((float)(Math.cos(newAngle) * velMagnitude),(float)(Math.sin(newAngle) * velMagnitude));
			pulse();
			p.shieldPulse();

			//This prevents overlapping the shield because otherwise the ball kinda pokes through and it bugs me
			float distanceToShift = MathUtil.distance(p.getPosition(), getPosition()) - p.getShieldDistance() + radius * getScale().x;
			getPosition().add((float)(Math.cos(newAngle) * distanceToShift),(float)(Math.sin(newAngle) * distanceToShift));
		}
		if(p.coreHits(this))
			playState.hit();
    }

    private void pulse()
	{
		getScale().set(1.5f, 1.5f);
		setBrightness(1);
		Tween.to(this, EntityAccessor.SCALE, .4f).target(1, 1).ease(TweenEquations.easeOutQuad).start(Tweener.getManager());
		Tween.to(this, EntityAccessor.BRIGHTNESS, .4f).target(0).ease(TweenEquations.easeOutQuad).start(Tweener.getManager());
	}

    private void testBallCollision()
	{
		for(Entity e: playState.getEntityHandler().getEntities())
		{
			if(e instanceof Ball && e != this)
			{
				Ball b = (Ball)e;
				if(MathUtil.distance(getPosition(), b.getPosition()) < radius * 2)
				{
					bounceOff(b.getPosition());
					b.bounceOff(getPosition());
					b.pulse();
					pulse();
					Locator.getAudio().play("ballHit","ballHit2","ballHit3");
				}
			}
		}
	}

	private void bounceOff(Vector2 pos)
	{
		float velMagnitude = MathUtil.magnitude(getVelocity());
		double newAngle = Math.atan2(getPosition().y - pos.y, getPosition().x - pos.x);
		newDirection(newAngle, velMagnitude);
	}

	private void newDirection(double radianAngle, float magnitude)
	{
		getVelocity().set((float)(Math.cos(radianAngle) * magnitude),(float)(Math.sin(radianAngle) * magnitude));
	}
	
	public void render(SpriteBatch sb, ShapeRenderer sr)
	{
		Gdx.gl.glLineWidth(3);
        sr.begin(ShapeType.Filled);
		sr.setColor(1, getBrightness(), getBrightness(), 1);
		sr.circle(getPosition().x, getPosition().y, radius * getScale().x);
		sr.end();
	}

}
