package com.carrot.deflect.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.carrot.deflect.util.render.ShapeRenderer;

import java.util.ArrayList;

public class EntityHandler {

	private ArrayList<Entity> entities = new ArrayList<Entity>();
	private ArrayList<Entity> entitiesToRemove = new ArrayList<Entity>();
	private ArrayList<Entity> entitiesToAdd = new ArrayList<Entity>();
	/**
	 * If true, all entities will be cleared on the next update
	 */
	private boolean clearQueued = false;
	
	/**
	 * Updates all entities, adds and removes those that are queued to be
	 * @param dt
	 */
	public void update(float dt)
	{
		if(clearQueued)
			doClear();
		else
		{
			//Add the entities that are queued to be added
			for(Entity e:entitiesToAdd)
				if(!entities.contains(e)&&!entitiesToRemove.contains(e))
				{
					entities.add(e);
					e.init();
				}
			entitiesToAdd.clear();

			for(Entity e:entities)
				e.update(dt);

			//Remove and destroy the entities that are queued to be destroyed
			entitiesToAdd.removeAll(entitiesToRemove);
			for(Entity e:entitiesToRemove)
			{
				e.destroy();
				e.dispose();
			}
			entities.removeAll(entitiesToRemove);
			entitiesToRemove.clear();
		}

	}

	public void render(SpriteBatch sb, ShapeRenderer sr)
    {
        for(Entity e: entities)
            e.render(sb, sr);
    }

	/**
	 * Disposes all entities on next update. Does not call their {@link Entity#destroy()} method
	 */
	public void clear()
	{
		clearQueued = true;
	}

	private void doClear()
	{
		for(Entity e:entities)
			e.dispose();
		for(Entity e:entitiesToAdd)
			e.dispose();
		for(Entity e:entitiesToRemove)
			e.dispose();

		entities.clear();
		entitiesToAdd.clear();
		entitiesToRemove.clear();
		clearQueued = false;
	}
	
	/**
	 * Queues the entity to be added when {@link PlayState#update()} is called
	 * @param e
	 */
	public void addEntity(Entity e)
	{
		if(!entitiesToAdd.contains(e))
			entitiesToAdd.add(e);
	}

	
	/**
	 * Queues the entity to be removed when {@link PlayState#update()} is called
	 * @param e
	 */
	public void removeEntity(Entity e)
	{
		if(!entitiesToRemove.contains(e)&&(entitiesToAdd.contains(e)||entities.contains(e)))
			entitiesToRemove.add(e);
	}

	/**
	 * 
	 * @return The list of currently existing enemies
	 */
	public ArrayList<Entity> getEntities() {
		return entities;
	}

}
