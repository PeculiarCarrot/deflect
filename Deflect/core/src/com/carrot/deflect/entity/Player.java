package com.carrot.deflect.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.carrot.deflect.Game;
import com.carrot.deflect.service.Locator;
import com.carrot.deflect.util.MathUtil;
import com.carrot.deflect.util.Tweener;
import com.carrot.deflect.util.render.ShapeRenderer;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;

public class Player extends Entity{

	/**
	 * The player's rotation in degrees
	 */
	private float angle = 0;
	/**
	 * The size (in degrees) of the shield's arc
	 */
	private float shieldSize = 180;
	/**
	 * The distance the shield will be from the player's center point
	 */
	private int shieldDistance = 45;
	/**
	 * This is the player's margin of error to deflect balls (in pixels), so they don't have to be 100% perfect
	 */
	private int deflectBuffer = 10;
	/**
	 * Applied to player's velocity
	 */
    private float fric = .98f;
	/**
	 * The player's acceleration rate
	 */
    private float moveSpeed = 10f;
	/**
	 * Radius of the player's core
	 */
    private int radius = 30;

	public Player(int x, int y) {
		super(x, y);
	}
	
	public void handleInput()
	{
		angle = (float) Math.toDegrees(Math.atan2(getPosition().y - Gdx.input.getY(), getPosition().x - Gdx.input.getX())) + 180;
	}

	public int getShieldDistance()
	{
		return shieldDistance;
	}
	
	public void update(float dt)
	{
        if((getPosition().x + radius >= Game.WIDTH && getVelocity().x > 0) || (getPosition().x -  radius <= 0 && getVelocity().x < 0))
            getVelocity().x *= -1;
        if((getPosition().y + radius >= Game.HEIGHT && getVelocity().y > 0) || (getPosition().y - radius <= 0 && getVelocity().y < 0))
            getVelocity().y *= -1;

        double angle = Math.atan2(getPosition().y - Gdx.input.getY(), getPosition().x - Gdx.input.getX());
       // if(Gdx.input.isTouched())
         //   getVelocity().add(-(float)(Math.cos(angle) * moveSpeed),-(float)(Math.sin(angle) * moveSpeed));

        getVelocity().scl(fric);
        getPosition().add(getVelocity().x * dt, getVelocity().y * dt);
	}

	public void shieldPulse()
	{
		getScale().set(2, 2);
		Tween.to(this, EntityAccessor.SCALE, .6f).target(1, 1).ease(TweenEquations.easeOutQuad).start(Tweener.getManager());
		Locator.getAudio().play("shieldHit");
	}

	public boolean shieldHits(Ball ball)
	{
		float dist = MathUtil.distance(getPosition(), ball.getPosition());
		float angleToBall = (float) Math.toDegrees(Math.atan2(getPosition().y - ball.getPosition().y, getPosition().x - ball.getPosition().x)) + 180;

		return (dist < shieldDistance + deflectBuffer && dist > shieldDistance - deflectBuffer && MathUtil.isDegreeAngleBetween(angleToBall, angle - shieldSize / 2, angle + shieldSize / 2));
	}

	public boolean coreHits(Ball ball)
	{
		float dist = MathUtil.distance(getPosition(), ball.getPosition());
		return dist < radius + ball.getRadius();
	}
	
	public void render(SpriteBatch sb, ShapeRenderer sr)
	{
		Gdx.gl.glLineWidth(3 * getScale().x);
		sr.begin(ShapeType.Filled);
		sr.setAutoShapeType(true);
		sr.setColor(getBrightness(), 1, getBrightness(), 1);
		sr.circle(getPosition().x, getPosition().y, radius);
		sr.end();
		
		sr.setAutoShapeType(true);
		sr.begin();
		sr.setColor(1, 1, 1, 1);
		sr.arc(getPosition().x, getPosition().y, shieldDistance * (getScale().x / 8 + 1), angle - shieldSize / 2, shieldSize, 20);

        if (Game.debug)
        {
            sr.setColor(0, 0, 1, 1);
            sr.line(getPosition().x, getPosition().y, Gdx.input.getX(), Gdx.input.getY());
        }
		
		sr.end();
	}

}
