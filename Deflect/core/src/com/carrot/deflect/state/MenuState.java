package com.carrot.deflect.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.carrot.deflect.Game;
import com.carrot.deflect.service.Locator;
import com.carrot.deflect.util.Fonts;
import com.carrot.deflect.util.render.ShapeRenderer;

public class MenuState extends State{
	
	private Texture logo;
	private float blinkTimer;
	private boolean renderingTouchText = true;

	public MenuState() {
		cam.setToOrtho(true, Game.WIDTH, Game.HEIGHT);
		logo = new Texture("textures/logo.png");
		//playButton = new Texture("playButton.jpg");
	}

	@Override
	public void handleInput() {
		if(Gdx.input.justTouched())
		{
			Locator.getStateManager().set(new PlayState());
		}
	}

	@Override
	public void update(float dt) {
		blinkTimer += dt;
		if(blinkTimer > .75f && renderingTouchText)
		{
			blinkTimer -= .75f;
			renderingTouchText = false;
		}
		else if(blinkTimer > .5f && !renderingTouchText)
		{
			blinkTimer -= .5f;
			renderingTouchText = true;
		}
	}

	@Override
	public void render(SpriteBatch sb, ShapeRenderer sr) {
		sb.setProjectionMatrix(cam.combined);
		sr.setProjectionMatrix(cam.combined);
		sb.begin();
		sr.setAutoShapeType(true);
		sr.begin();
		sb.draw(logo, Game.WIDTH / 2 - logo.getWidth() / 2,  Game.HEIGHT / 2, logo.getWidth(), -logo.getHeight());
		//sb.draw(playButton, Game.WIDTH/2 - playButton.getWidth()/2, Game.HEIGHT/2 - playButton.getHeight()/2);
		if(renderingTouchText)
		{
			GlyphLayout glyphLayout = new GlyphLayout();
			glyphLayout.setText(Fonts.score, "Touch to begin");
			float w = glyphLayout.width;
			Fonts.score.draw(sb, "Touch to begin", cam.position.x - w / 2, cam.position.y + 40);
		}
		sb.end();
		sr.end();
	}

	@Override
	public void dispose() {
		logo.dispose();
		//playButton.dispose();
	}

}
