package com.carrot.deflect.state;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.carrot.deflect.util.render.ShapeRenderer;

public abstract class State {
	protected OrthographicCamera cam;
	protected Vector2 mouse;
	
	public State()
	{
		cam = new OrthographicCamera();
		mouse = new Vector2();
	}
	
	public abstract void handleInput();
	public abstract void update(float dt);
	public abstract void render(SpriteBatch sb, ShapeRenderer sr);
	public abstract void dispose();
	
	/**
	 * Called when this is set as the current state
	 */
	public void onEnterState()
	{
		
	}
	
	/**
	 * Called right before this state is being changed to another
	 */
	public void onExitState()
	{
		
	}
}
