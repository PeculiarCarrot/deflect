package com.carrot.deflect.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.carrot.deflect.Game;
import com.carrot.deflect.entity.Ball;
import com.carrot.deflect.entity.EntityHandler;
import com.carrot.deflect.entity.Player;
import com.carrot.deflect.service.Locator;
import com.carrot.deflect.util.Fonts;
import com.carrot.deflect.util.Tweener;
import com.carrot.deflect.util.render.ShapeRenderer;

import aurelienribon.tweenengine.TweenManager;

public class PlayState extends State{
	
	private Player player;
    private EntityHandler entityHandler;
    private float newBallTimer = 0;
    private float secondsPerSpawn = 4f;
    private float difficulty = 1;
	private int score = 0;
	private boolean hit = false;
	private float secondsSinceHit = 0;
	private TweenManager tweenManager;

	public PlayState() {
		cam.setToOrtho(true, Game.WIDTH, Game.HEIGHT);
        cam.position.x = Game.WIDTH / 2;
        cam.position.y = Game.HEIGHT / 2;
        entityHandler = new EntityHandler();
		restart();
	}

	public void restart()
	{
		player = new Player(Game.WIDTH / 2, Game.HEIGHT / 2);
		entityHandler.clear();
		score = -1;
		hit = false;
		secondsSinceHit = 0;
		newBallTimer = 0;
	}

	public void hit()
	{
		if(!hit)
		{
			Game.updateScore(score);
			hit = true;
			Locator.getAudio().play("die","die2","die3");
		}
	}

	@Override
	public void handleInput() {
		if(!hit)
			player.handleInput();
		else if(Gdx.input.justTouched() && secondsSinceHit > 1f)
			restart();
	}

	private void spawnNewBall()
    {
        newBallTimer = secondsPerSpawn;
        int x = Math.random() < .5 ? -20 : Game.WIDTH + 20;
        int y = Math.random() < .5 ? -20 : Game.HEIGHT + 20;
        Ball ball = new Ball(x, y, this);
		score++;
        entityHandler.addEntity(ball);
    }

    public float getDifficulty()
    {
        return difficulty;
    }

    public Player getPlayer()
    {
        return player;
    }
	
	/**
	 * Handles input, game logic, etc.
	 */
	public void update(float dt) {
		if(!hit)
		{
			Tweener.getManager().update(dt);
			if(newBallTimer <= 0)
				spawnNewBall();
			else
				newBallTimer -= dt;

			difficulty += .0001f * dt;

			entityHandler.update(dt);

			if(entityHandler.getEntities().size() == 0)
			{
				entityHandler.addEntity(player);
			}

			cam.update();
		}
		else
			secondsSinceHit += dt;
	}

    public EntityHandler getEntityHandler()
	{
		return entityHandler;
	}

	@Override
	public void render(SpriteBatch sb, ShapeRenderer sr) {
        sr.setProjectionMatrix(cam.combined);
        sb.setProjectionMatrix(cam.combined);
		entityHandler.render(sb, sr);
		player.render(sb, sr);
		sb.begin();
		GlyphLayout glyphLayout = new GlyphLayout();
		glyphLayout.setText(Fonts.score, "Score: ");
		float w = glyphLayout.width;
		Fonts.score.draw(sb, "Score: "+ score, cam.position.x - w / 2, cam.position.y - cam.viewportHeight / 2 + 130);

		sb.end();
		if(hit)
		{
			sb.begin();
			glyphLayout.setText(Fonts.score, "High Score: ");
			w = glyphLayout.width;
			Fonts.score.draw(sb, "High Score: "+ Game.getHighScore(), cam.position.x - w / 2, cam.position.y - cam.viewportHeight / 2 + 190);

			glyphLayout.setText(Fonts.score, "Touch to play again!");
			w = glyphLayout.width;
			Fonts.score.draw(sb, "Touch to play again!", cam.position.x - w / 2, cam.position.y + 200);
			sb.end();

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			//Flash the screen when we die
			sr.begin();
			sr.set(com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType.Filled);
			float alpha = MathUtils.clamp(-secondsSinceHit * 1.8f + 1, 0, 1);
			sr.setColor(.8f, 0, 0, alpha);
			sr.rect(0, 0, Game.WIDTH, Game.HEIGHT);
			sr.end();

			Gdx.gl.glDisable(GL20.GL_BLEND);
		}
	}

	@Override
	public void dispose() {
		
	}

}
