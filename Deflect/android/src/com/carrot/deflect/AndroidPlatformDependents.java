package com.carrot.deflect;

import com.carrot.deflect.service.PlatformDependents;

/**
 * Created by aharm on 9/22/2017.
 */

public class AndroidPlatformDependents extends PlatformDependents{

    public AndroidPlatformDependents()
    {
        logger = new AndroidLogger();
        platform = "android";
        debug = false;
    }
}
