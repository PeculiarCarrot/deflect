package com.carrot.deflect;

import com.carrot.deflect.service.Logger;

import java.util.Calendar;
import android.util.Log;

/**
 * Created by aharm on 9/22/2017.
 */

public class AndroidLogger extends Logger {

    public void log(String log)
    {
        Log.d("DEBUG", log);
    }

    public void error(String error)
    {
        Log.e("ERROR", error);
        throwError();
    }

    public void warn(String warn)
    {
        Log.i("INFO", warn);
    }

    public void throwError()
    {
        new Exception().printStackTrace();
    }
}
