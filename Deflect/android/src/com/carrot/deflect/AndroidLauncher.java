package com.carrot.deflect;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AndroidLauncher extends AndroidApplication {

	private static final String TAG = "AndroidLauncher";
	protected AdView adView;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		RelativeLayout layout = new RelativeLayout(this);

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.numSamples = 4;
		Game game = new Game(new AndroidPlatformDependents());
		View gameView = initializeForView(game, config);
		layout.addView(gameView);

		adView = new AdView(this);

		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setBackgroundColor(Color.TRANSPARENT);
		adView.setAdUnitId("ca-app-pub-9961428133196748/2717159034");

		AdRequest.Builder builder = new AdRequest.Builder();
		builder.addTestDevice("B5CD859877A8ECA83A0938D2E27FA89C");
		RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT
		);

		layout.addView(adView, adParams);
		adView.loadAd(builder.build());

		setContentView(layout);
	}
}
